var RECOND_INFOMATION_INDEX = {
    'STATUS' : 3,
    'COMMENT' : 8,
}

function setPopover(content){
    // popover setting.
    var options = {
	title : "record information",
	trigger: "manual",
	placement: 'left',
	content: getQuickRecordInformationTable ,
	html : true
    };
    $("#quickRecordInformation").popover(options);
}

function syncRecordInformation(e){
    var quickRecordInformationTrs = $("#quickRecordInformationMenu").find(".popover-content > p > tbody").children("tr");
    var recordInformationTrs = $(".collectionmodule_table").children("tbody").children("tr");

    console.log(quickRecordInformationTrs);
    console.log(recordInformationTrs);

    // sync data
    $( recordInformationTrs[RECOND_INFOMATION_INDEX['STATUS']] ).replaceWith( $(quickRecordInformationTrs[0]).clone() );
    $( recordInformationTrs[RECOND_INFOMATION_INDEX['COMMENT']] ).replaceWith( $(quickRecordInformationTrs[1]).clone() );
}

function setSyncData(enabled){    
    if(enabled){
	// cover Status:input & Comment. 
	$("#quickRecordInformationMenu").children(".popover").on("DOMSubtreeModified", syncRecordInformation);
	// cover Status:select.
	$("#quickRecordInformationMenu").children(".popover").on("click",'select',syncRecordInformation);
    } else {
	// cover Status:input & Comment. 
	$("#quickRecordInformationMenu").children(".popover").off("DOMSubtreeModified", syncRecordInformation);
	// cover Status:select.
	$("#quickRecordInformationMenu").children(".popover").off("click",'select',syncRecordInformation);	
    }
}

function popover(){
    var popover = $("#quickRecordInformationMenu").children(".popover");
    var quickRecordInformation = $("#quickRecordInformation");
    if ( popover.length ){
	setSyncData(false);	
        quickRecordInformation.popover('hide');
    } else {
	quickRecordInformation.popover('show');
	setSyncData(true);
    }
}

function getQuickRecordInformationTable(){
    var table = $(".collectionmodule_table").clone();
    table.empty();

    var trs = $(".collectionmodule_table").children("tbody").children("tr").clone();
    table.append( trs[RECOND_INFOMATION_INDEX['STATUS']] ); 
    table.append( trs[RECOND_INFOMATION_INDEX['COMMENT']] );

    return table.html();
}

function getRecordInformationLink(){
    var module = $("div:has(#collection_status)");
    var recordInfomationLink = module.find("a:contains('Record information')");
    return recordInfomationLink;
}

function clickRecordInfomationLink(){
    var link = getRecordInformationLink();
    link.trigger('click');    
}

function popoverAfterAjaxProcessComplete(counter){
    var MaxAjaxProcessCompleteWaitCount = 5;
    var collectionmodule_table = $(".collectionmodule_table");
    if ( collectionmodule_table.length > 0 ){
	popover();
    } else {
	if (  counter > MaxAjaxProcessCompleteWaitCount){
	    // timeout 
	} else {	    
	    window.setTimeout(new function(c){
 				  return function(){ popoverAfterAjaxProcessComplete(c) };
 			      }(counter+1), 1000);
	}
    }
}

function popoverRecordInformation( ) {
    // check record information table
    var collectionmodule_table = $(".collectionmodule_table");    
    if ( collectionmodule_table.length > 0 ){
	// table exists.
	popover();
    } else {
	// table not exists.
	clickRecordInfomationLink();
	popoverAfterAjaxProcessComplete(0);
    }
}

function addPopoverButton(func){
    var insertElement = document.getElementById('maincontent');

    // insert item.
    var menubar = document.createElement("div");
    menubar.id = "quickRecordInformationMenu";

    var popoverButton = document.createElement("div");
    popoverButton.id = "quickRecordInformation";
    popoverButton.setAttribute("style", "background-image: url(" + chrome.extension.getURL("img/plus48x48.png") + ");");
    popoverButton.setAttribute("rel","popover");
    popoverButton.addEventListener("click", popoverRecordInformation);
    
    menubar.appendChild(popoverButton);
    insertElement.insertBefore(menubar, insertElement.firstChild);
}

// onload setting.
if (document.readyState == "complete") {   
    addPopoverButton();
    setPopover();
} else {    
    addPopoverButton();
    setPopover();
}
